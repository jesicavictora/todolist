<?php

namespace ToDoList;

class Done
{

    public function __construct($taskId)
    {

        $str_datos = file_get_contents("todo.json");
        $todolist = json_decode($str_datos, true);

        foreach ($todolist as $i => $task) {

            if ($task['id'] == $taskId) {

                $todolist[$i]['status'] = true;
            }
        };

        $fh = fopen("todo.json", 'w')
            or die("Error al abrir fichero de salida");
        fwrite($fh, json_encode($todolist, JSON_UNESCAPED_UNICODE));
        fclose($fh);

        $newURL = "http://localhost:8888/index.php";
        header('Location: ' . $newURL);
    }
}
