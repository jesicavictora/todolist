<?php


namespace ToDoList;

class AddTask
{

    public function __construct($newTask)
    {

        $str_datos = file_get_contents("todo.json");
        $todolist = json_decode($str_datos, true);
        $guardar = [];
        $guardar['id'] = hash('md5', date('Ymdhisv'));
        $guardar['title'] = $newTask;
        $guardar['status'] = false;

        array_push($todolist, $guardar);

        $fh = fopen("todo.json", 'w')
            or die("Error al abrir fichero de salida");
        fwrite($fh, json_encode($todolist, JSON_UNESCAPED_UNICODE));
        fclose($fh);

        $newURL = "http://localhost:8888/index.php";
        header('Location: ' . $newURL);
    }
}
