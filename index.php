<?php

$str_datos = file_get_contents("todo.json");
$todolist = json_decode($str_datos, true);


?>

<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>ToDoList</title>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
    <link href="src/style.css" rel="stylesheet">
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js'></script>

</head>

<body oncontextmenu='return false' class='snippet-body'>

    <div class="page-content page-container" id="page-content">
        <div class="padding">
            <div class="row container d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="card px-3">
                        <div class="card-body">
                            <h4 class="card-title">Awesome Todo list</h4>
                            <form enctype="multipart/form-data" method="POST" action="add.php">
                                <div class="add-items d-flex">
                                    <input type="text" class="form-control todo-list-input" name="addTask" placeholder="What do you need to do today?">
                                    <button class="add btn btn-primary font-weight-bold todo-list-add-btn">Add</button>
                                </div>
                            </form>
                            <div class="list-wrapper">
                                <ul class="d-flex flex-column-reverse todo-list">
                                    <?php

                                    foreach ($todolist as $task) {

                                        $li = '';

                                        if ($task['status']) {
                                            $li = ' <li class="completed">
                                            <div class="form-check">
                                        <label class="form-check-label">
                                             <button class="blue"></button>';
                                        } else {
                                            $li = ' <li> <div class="form-check">
                                           
                                            <form enctype="multipart/form-data" method="POST" action="done.php" >
                                            <label class="form-check-label">
                                            <input type="hidden" name="id" value="' . $task['id'] . '">
                                            <button class="white"></button>
                                            </form>
                                            ';
                                        };

                                        $li .= $task['title'] . '
                                            <i class="input-helper"></i>
                                        </label>
                                    </div>
                                    <form enctype="multipart/form-data" method="POST" action="remove.php" style=" margin-left: auto !important;">
                                    <input type="hidden" name="id" value="' . $task['id'] . '">
                                        <button class="remove">X</button>
                                        </form>
                         
                                </li>';

                                        echo $li;
                                    };

                                    ?>


                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>